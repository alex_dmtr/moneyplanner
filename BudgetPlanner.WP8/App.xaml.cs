﻿namespace BudgetPlanner.WP8
{
    using System.Diagnostics;

    using Microsoft.Phone.Shell;
    using Microsoft.Phone.Controls;

    public partial class App
    {
        public static PhoneApplicationFrame RootFrame { get; private set; }

        public App()
        {
            InitializeComponent();

            if ( Debugger.IsAttached )
            {
                Current.Host.Settings.EnableFrameRateCounter = true;

                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

        }
    }
}