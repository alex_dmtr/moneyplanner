namespace BudgetPlanner.WP8.ViewModels
{
    public class MainViewModel
    {
        public MainViewModel()
        {
            PageName = "It worked!";
        }

        public string PageName { get; set; }
    }
}